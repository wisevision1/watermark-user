const router = require('express').Router();
const fs = require('fs');
const multer  = require('multer');

//const controller = require('../controllers/proceed').run;
const controller = require('../controllers/proceed2').run;

const storage = multer.diskStorage({
    destination: async (req, file, cb) => { 
        const directory = `controllers/image/to_parse/${req.body.user_name}`;
        if (!fs.existsSync(directory)) await fs.mkdirSync(directory);
        await cb(null, directory); 
    },
    filename: (req, file, cb) => { cb(null, file.originalname); }
});
const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') cb(null, true);
    else cb(null, false);
};
const upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 11 /* 11 MB */ },
    fileFilter: fileFilter
});

// multer section
router.post('/upload-image',  upload.single('file') , async (req, res) => {
    controller(req.file.destination, req.file.filename, req.body.user_name, res);
});

module.exports = router;