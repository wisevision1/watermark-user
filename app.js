const express = require('express');
const http = require('http');
//const https = require('https');
const cors = require('cors');

const app = express();
app.use(cors());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/', require('./routes/route') );

app.use('/',express.static(__dirname + '/views'));
app.use('/images',express.static(__dirname + '/controllers/image/parsed'));

http.createServer(app).listen(8000);