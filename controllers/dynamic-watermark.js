const fs = require('fs');
const watermark = require('dynamic-watermark');

const host = require('../configs/adress').domain;

exports.run = async (source_name, source_path, source_dimensions, watermark_path_name, user_name, res ) => {

    const directory = `controllers/image/parsed/${user_name}`;
    if (!fs.existsSync(directory)) await fs.mkdirSync(directory);

    const optionsImageWatermark = {
        type: "image",
        source: `${source_path}/${source_name}`, 
        logo:watermark_path_name, 
        destination: `controllers/image/parsed/${user_name}/${source_name}`, 
        position: {
            logoX : source_dimensions.width - 200,
            logoY : source_dimensions.height - 100,
            logoHeight: 100,
            logoWidth: 200
        }
    };
    
    await watermark.embed(optionsImageWatermark, async (status) => {

        if (status.status === 1) 
            res.send({
                success: true, 
                url: `${host}images/${user_name}/${source_name}`
            });
        else res.send({
            success: false, 
            text: 'Something gores wrong!'
        });
    });
};

