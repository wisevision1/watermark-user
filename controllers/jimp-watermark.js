const watermark = require('jimp-watermark');

exports.run = async (filepath, short_filename, username) => {
    
    const options = {
        'ratio': 0.5,
        'opacity': 0.6, 
        'dstPath' : `controllers/image/parsed/${username}/${short_filename}.png`
    };
    watermark.addWatermark(filepath, `controllers/image/watermark/${username}/${short_filename}.png`, options)
    .then(data => {
        //
    }).catch(err => {
        //
    });
};
