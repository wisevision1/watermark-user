const { promisify } = require('util');
const sizeOf = promisify(require('image-size'));

exports.getImageSizes = async (path) => {
    return await sizeOf(path)
    .then(dimensions => { 
        return dimensions;
    }).catch(err => console.error(err));
};
