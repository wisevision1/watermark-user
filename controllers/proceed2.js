
const text2png = require('./text2png').run;
const image_size = require('./image-size').getImageSizes;
const dynamic_watermark = require('./dynamic-watermark').run;

exports.run = async (filepath, full_filename, username, res) => {

    const [short_filename, extension] = full_filename.split('.');
    
    await text2png(short_filename, username)
    .then( async () => {

        await image_size(filepath +'/' + full_filename)
        .then( async dimensions => {

            await dynamic_watermark(
                full_filename, 
                filepath, 
                dimensions,
                `controllers/image/watermark/${username}/${short_filename}.png`,
                username, res);

        }).catch(err => console.error(err));
    }).catch(err => console.error(err));
};