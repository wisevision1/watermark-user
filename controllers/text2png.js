const fs = require('fs');
const text2png = require('text2png');

const options = {
    font: '80px Becker',
    localFontPath: 'controllers/fonts/becker.shadow.ttf',
    localFontName: 'Becker',
    color: '#80FFFFFF',
    backgroundColor: 'transparent',
    lineSpacing: 10,
    padding: 20
};

exports.run = async (filename, username) => {
    const directory = `controllers/image/watermark/${username}`;
    if (!fs.existsSync(directory)) await fs.mkdirSync(directory);

    fs.writeFileSync(
        `${directory}/${filename}.png`, 
        text2png(username, options));
};

