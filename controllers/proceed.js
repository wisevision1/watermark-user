
const text2png = require('./text2png').run;
const jimp_w = require('./jimp-watermark').run;

const host = require('../configs/adress').domain;

exports.run = async (filepath, full_filename, username, res) => {

    const [short_filename, extension] = full_filename.split('.');
    await text2png(short_filename, username)
    .then( async () => {
        await jimp_w(filepath, short_filename, username)
        .then( async () => {
            const url = `${host}images/${username}/${short_filename}.${extension}`;

            res.send({success: true, url: url});
        });
    });
};